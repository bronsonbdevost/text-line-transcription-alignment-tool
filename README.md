# Line Match

A tool to align transcribed lines with corresponding regions on an image.

## The executables

This repository also provides compiled executables that should run on most Windows/Mac/Linux systems.

- [Windows installer](./releases/windows/Line%20Match%20Setup%200.1.0.exe)
- [Mac installer](./releases/mac/Line%20Match-0.1.0.dmg)
- [Linux Alpine](./releases/linux/line-match-0.1.0.apk)
- [Linux Debian/Ubuntu](./releases/linux/line-match_0.1.0_amd64.deb)
- [Linux RPM](./releases/linux/line-match-0.1.0.x86_64.rpm)
- [FreeBSD](./releases/linux/line-match-0.1.0.freebsd)

## Usage

Coming soon...

## Output

We do not yet have syntactically correct ALTO output, for now the application JSON file output should be used for further computation. 

The `open` and `save` file menu options make use of a simple JSON file with the following schema:

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title" : "Line Match JSON file format",
  "type": "object",
  "properties": {
    "projectName": {
      "type": "string",
      "description" : "Name of the current project."
    },
    "imageDesignation": {
      "type": "string",
      "description" : "Name of the image being annotated."
    },
    "imageUrl": {
      "type": "string",
      "description" : "Url of the image being annotated. This can be a file path, or the url to a IIIF image asset."
    },
    "mappings": {
      "type": "array",
      "description" : "An array of the objects with matched sets of polyline regions on the image and the corresponding transcription."
      "items": [
        {
          "type": "object",
          "properties": {
            "polyline": {
              "type": "array",
              "description" : "An array of points (in the form of [xLocation, yLocation]) that construct a polyline."
              "items": [
                {
                  "type": "array",
                  "items": [
                    {
                      "type": "integer",
                      "description" : "The x coordinate of the point."
                    },
                    {
                      "type": "integer",
                      "description" : "The y coordinate of the point."
                    }
                  ]
                }
              ]
            },
            "transcribedLine": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string",
                  "description" : "A name given to the line."
                },
                "text": {
                  "type": "string",
                  "description" : "The text transcription for the marked line."
                }
              },
              "required": [
                "name",
                "text"
              ]
            }
          },
          "required": [
            "polyline",
            "transcribedLine"
          ]
        }
      ]
    }
  },
  "required": [
    "projectName",
    "imageDesignation",
    "imageUrl",
    "mappings"
  ]
}
```

## Development

### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn electron:serve
```

### Compiles and minifies for production
```
yarn electron:build
```

To compile executables on Mac as releases for all platforms: `./build-releases.sh`

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
