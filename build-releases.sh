export CSC_IDENTITY_AUTO_DISCOVERY=false
yarn electron:crossbuild
cp dist_electron/line-match_*_amd64.snap releases/linux/
cp dist_electron/line-match_*_amd64.deb releases/linux/
cp dist_electron/line-match-*.apk releases/linux/
cp dist_electron/line-match-*.freebsd releases/linux/
cp dist_electron/line-match-*.x86_64.rpm releases/linux/
cp dist_electron/Line\ Match-*-win.zip releases/windows/
cp dist_electron/Line\ Match\ Setup\ *.exe releases/windows/
cp dist_electron/Line\ Match-*.dmg releases/mac/
cp dist_electron/Line\ Match-*-mac.tar.gz releases/mac/