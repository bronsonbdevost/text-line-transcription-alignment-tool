// vue.config.js

module.exports = {
	pluginOptions: {
		electronBuilder: {
			builderOptions: {
				"mac": {
					"category": "public.app-category.utilities",
					"target": [
						"tar.gz",
						"dmg"
					],
					"icon": "icons/mac/app.icns"
				},
				"win": {
					"target": [
						"nsis"
					],
					"icon": "icons/win/app.ico"
				},
				"linux": {
					"target": [
						"AppImage",
						"snap",
						"deb",
						"rpm",
						"freebsd",
						"apk"
					],
					"icon": "icons/png"
				}
			}
		}
	}
}