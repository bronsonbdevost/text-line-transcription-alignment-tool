import axios from 'axios'

const url = "https://api.qumranica.org"
const version = "v1"
const editionsPath = `${url}/${version}/editions`

const textFragmentNamesPath = (editionId) => `${editionsPath}/${editionId}/text-fragments`
const textFragmentTranscriptionPath = (editionId, textFragmentId) => `${textFragmentNamesPath(editionId)}/${textFragmentId}`

const getPath = async (path) => {
	try {
		const res = await axios.get(path)
		return res.data
	} catch(err) {
		alert("Error retrieving text: ", err)
	}
}

export const getManuscripts = async () => {
	const response = await getPath(editionsPath)
	return ([].concat(...response.editions)).map(x => { return {id: x.id, name: x.name} })
}

export const getManuscriptTextFragments = async (editionId) => {
	const response = await getPath(textFragmentNamesPath(editionId))
	return response.textFragments
}

export const getManuscriptTextFragmentTranscription = async (editionId, textFragmentId) => {
	return await getPath(textFragmentTranscriptionPath(editionId, textFragmentId))
}