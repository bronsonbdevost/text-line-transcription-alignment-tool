export const formatTranscription = (transcriptionObject) => {
	let transcription = ''
	let lineNames = []

	for (const textFragment of transcriptionObject.textFragments) {
		for (const line of textFragment.lines) {
			for (const sign of line.signs) {
				for (const signInterpretation of sign.signInterpretations) {
					const attributes = signInterpretation.attributes.map(x => x.attributeValueId)
					if (attributes.indexOf(10) !== -1){
						if (transcription !== '') transcription += '\n'
						lineNames.push(line.lineName)
					}
					if (attributes.indexOf(5) !== -1) transcription += '\t'
					if (attributes.indexOf(9) !== -1) transcription += '\t'
					if (attributes.indexOf(20) === -1) {
						if (attributes.indexOf(1) !== -1) transcription += signInterpretation.character
						else if (attributes.indexOf(2) !== -1) transcription += ' '
					}
				}
			}
		}
	}

	return [transcription, lineNames]
}

export default formatTranscription