export const loadData = (dataObject, mappedLines) => {
	unloadOldData(mappedLines)
	mappedLines.projectName = dataObject.projectName
	mappedLines.imageDesignation = dataObject.imageDesignation
	mappedLines.imageUrl = dataObject.imageUrl

	for (let [idx, mapping] of dataObject.mappings.entries()) {
		if (mapping.polyline !== null) mappedLines.svgLinesList.push({svgPolyLine: mapping.polyline})
		if (mapping.transcribedLine !== null) {
			mappedLines.transcribedLines[idx] = mapping.transcribedLine
			mappedLines.transcribedLinesList.push(idx)
		}
	}
}

export const unloadOldData = (mappedLines) => {
	mappedLines.projectName = undefined
	mappedLines.imageDesignation = undefined
	mappedLines.imageUrl = undefined
	mappedLines.selectedLine = Object.assign({},{svg: undefined, transcrip: undefined})
	mappedLines.imageHeight = undefined
	mappedLines.imageWidth = undefined
	unloadTranscribedLines(mappedLines)
	mappedLines.svgLinesList = []
}

export const unloadTranscribedLines = (mappedLines) => {
	mappedLines.transcribedLines = {}
	mappedLines.transcribedLinesList = []
}

export default loadData