const prepareData = (mappedLines) => {
	const longerListLength = mappedLines.svgLinesList.length > mappedLines.transcribedLinesList.length
		? mappedLines.svgLinesList.length : mappedLines.transcribedLinesList.length
	let combinedMappings = []
	for (let idx = 0; idx < longerListLength; idx++) {
		combinedMappings.push({
														polyline: idx < mappedLines.svgLinesList.length
															? mappedLines.svgLinesList[idx].svgPolyLine : null,
														transcribedLine: idx < mappedLines.transcribedLinesList.length
															? mappedLines.transcribedLines[mappedLines.transcribedLinesList[idx]]
															: null})
	}

	return {
						projectName: mappedLines.projectName,
						imageDesignation: mappedLines.imageDesignation,
						imageUrl: mappedLines.imageUrl,
						mappings: combinedMappings,
					}
}

const getJsonData = (mappedLines) => {
	return JSON.stringify(prepareData(mappedLines))
}

const getXmlData = (mappedLines) => {
	let preparedJson = prepareData(mappedLines)
	return xmlBoilerPlate.header + 
		xmlBoilerPlate.pageStart((preparedJson.imageDesignation || '').replace(/ /g, '_'), preparedJson.imageHeight || 0, preparedJson.imageWidth || 0) +
		preparedJson.mappings.map(x => xmlBoilerPlate.line(x.transcribedLine, x.polyline !== null ? x.polyline.join(' ') : '')).join() +
		xmlBoilerPlate.pageEnd + xmlBoilerPlate.footer
}

const prepareFileData = (mappedLines, type) => {
	let blob, mimetype, extension

	if (type === 'JSON') {
		const data = getJsonData(mappedLines)
		mimetype = 'application/json'
		blob = new Blob([data], {type: mimetype})
		extension = 'json'
	}
	else if (type === 'XML') {
		const data = getXmlData(mappedLines)
		mimetype = 'text/xml'
		blob = new Blob([data], {type: mimetype})
		extension = 'xml'
	}
	return [blob, mimetype, extension]
}

const saveDataFile = (mappedLines, type) => {
	const [blob, mimetype, extension] = prepareFileData(mappedLines, type)
	const e = document.createEvent('MouseEvents')
	const a = document.createElement('a')
	a.download = `${mappedLines.projectName}.${extension}`
	a.href = window.URL.createObjectURL(blob)
	a.dataset.downloadurl = [mimetype, a.download, a.href].join(':')
	e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
	a.dispatchEvent(e)
}

const xmlBoilerPlate = {
	header: `<?xml version="1.0" encoding="UTF-8"?>
<alto	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns="http://www.loc.gov/standards/alto/ns-v4#"
		xsi:schemaLocation="http://www.loc.gov/standards/alto/ns-v4# http://www.loc.gov/standards/alto/v4/alto-4-0.xsd">

<Layout>`,
	footer: `
</Layout>

</alto>`,
	pageStart: (id, width, height) => `
	<Page PHYSICAL_IMG_NR="0" WIDTH="${width}" HEIGHT="${height}" ID="${id}">
		<PrintSpace HPOS="0" VPOS="0" WIDTH="${width}" HEIGHT="${height}">

			<TextBlock ID="dummyblock_${id}">`,
	pageEnd: `
			</TextBlock>

		</PrintSpace>
	</Page>`,
	line: (text, dims) => `
				<TextLine ID="${text.name}" POLYLINE="${dims}">
					<String POLYLINE="${dims}"
						CONTENT="${text.text || ''}"></String>
				</TextLine>`
}

export default saveDataFile